package com.elegoo.smartrobotcarcontroller.bluetooth

import android.bluetooth.*
import android.content.*
import android.os.Handler
import android.os.IBinder
import com.elegoo.smartrobotcarcontroller.bluetooth.BluetoothLeService.EXTRA_DATA
import com.elegoo.smartrobotcarcontroller.common.DEVICE_SCAN_TIME
import com.elegoo.smartrobotcarcontroller.common.SingletonHolder
import java.util.*


class BluetoothManager private constructor(val context: Context) {

    companion object : SingletonHolder<BluetoothManager, Context>(::BluetoothManager)

    private val LIST_NAME = "NAME"
    private val LIST_UUID = "UUID"

    private val bluetoothAdapter: BluetoothAdapter
    private var bluetoothLeService: BluetoothLeService? = null

    private var isConnected = false

    lateinit var deviceAddress: String

    private var characteristicTX: BluetoothGattCharacteristic? = null
    private var characteristicRX: BluetoothGattCharacteristic? = null


    init {
        val bluetoothManager = context.getSystemService(Context.BLUETOOTH_SERVICE) as android.bluetooth.BluetoothManager
        bluetoothAdapter = bluetoothManager.adapter
    }

    fun isBluetoothEnabled(): Boolean {
        return bluetoothAdapter.isEnabled
    }

    fun startScanBluetoothDevices() {
        Handler().postDelayed({
            stopScanBluetoothDevices()
        }, DEVICE_SCAN_TIME)
        bluetoothAdapter.startLeScan(leScanCallback)
        onScanStartedListener?.invoke()
    }

    fun stopScanBluetoothDevices() {
        bluetoothAdapter.stopLeScan(leScanCallback)
        onScanFinishedListener?.invoke()
    }

    private val leScanCallback = BluetoothAdapter.LeScanCallback { device, _, _ ->
        onDeviceFoundListener?.invoke(device)
    }

    var onScanStartedListener: (() -> Unit)? = null
    var onScanFinishedListener: (() -> Unit)? = null
    var onDeviceFoundListener: ((BluetoothDevice) -> Unit)? = null


    val serviceConnection = object : ServiceConnection {

        override fun onServiceConnected(componentName: ComponentName, service: IBinder) {
            bluetoothLeService = (service as BluetoothLeService.LocalBinder).service
            if (!bluetoothLeService!!.initialize()) {
                onBluetoothLeServiceInitFailed?.invoke()
            }
            connectLeService()
        }

        override fun onServiceDisconnected(componentName: ComponentName) {
            bluetoothLeService = null
        }
    }

    val gattUpdateReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            when (action) {
                BluetoothLeService.ACTION_GATT_CONNECTED -> {
                    isConnected = true
                    onGattConnectedListener?.invoke()
                }
                BluetoothLeService.ACTION_GATT_DISCONNECTED -> {
                    isConnected = false
                    onGattDisconnectedListener?.invoke()
                }
                BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED ->
                    displayGattServices(bluetoothLeService!!.supportedGattServices)
                BluetoothLeService.ACTION_DATA_AVAILABLE ->
                    onCharacteristicChanged!!.invoke(intent.getStringExtra(EXTRA_DATA))
            }
        }
    }

    fun connectLeService() {
        if (bluetoothLeService != null) {
            bluetoothLeService!!.connect(deviceAddress)
        }
    }

    fun disconnectLeService() {
        bluetoothLeService = null
    }

    fun sendCommand(angle: Int, speed: Int) {
        if (isConnected) {
            val message = String.format("<%03d,%03d>", angle, speed)
            val tx = message.toByteArray()
            characteristicTX!!.value = tx
            bluetoothLeService!!.writeCharacteristic(characteristicTX)

            val descriptor = characteristicRX!!.getDescriptor(
                    UUID.fromString(SampleGattAttributes.CLIENT_CHARACTERISTIC_CONFIG))
            descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
            bluetoothLeService!!.setCharacteristicNotification(characteristicRX, true)
        }
    }

    private fun displayGattServices(gattServices: List<BluetoothGattService>?) {
        if (gattServices == null) return
        var uuid: String
        val unknownServiceString = "Unknown service"
        val gattServiceData = ArrayList<HashMap<String, String>>()

        // Loops through available GATT Services.
        for (gattService in gattServices) {
            val currentServiceData = HashMap<String, String>()
            uuid = gattService.uuid.toString()
            currentServiceData[LIST_NAME] = SampleGattAttributes.lookup(uuid, unknownServiceString)
            currentServiceData[LIST_UUID] = uuid
            gattServiceData.add(currentServiceData)

            // get characteristic when UUID matches RX/TX UUID
            characteristicTX = gattService.getCharacteristic(BluetoothLeService.UUID_HM_RX_TX)
            characteristicRX = gattService.getCharacteristic(BluetoothLeService.UUID_HM_RX_TX)
        }
    }

    var onBluetoothLeServiceInitFailed: (() -> Unit)? = null
    var onGattConnectedListener: (() -> Unit)? = null
    var onGattDisconnectedListener: (() -> Unit)? = null
    var onCharacteristicChanged: ((value: String) -> Unit)? = null
}