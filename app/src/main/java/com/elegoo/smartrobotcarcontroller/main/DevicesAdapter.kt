package com.elegoo.smartrobotcarcontroller.main

import android.bluetooth.BluetoothDevice
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import java.util.*


class DevicesAdapter(private val listener: (BluetoothDevice) -> Unit):
        RecyclerView.Adapter<DevicesAdapter.BluetoothDeviceViewHolder>()  {

    private val items: ArrayList<BluetoothDevice> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BluetoothDeviceViewHolder {
        val deviceItem = LayoutInflater.from(parent.context)
                .inflate(android.R.layout.simple_selectable_list_item, parent, false) as TextView
        return BluetoothDeviceViewHolder(deviceItem)
    }

    override fun onBindViewHolder(holder: BluetoothDeviceViewHolder, position: Int) {
        val device = items[position]

        holder.textView.setTypeface(holder.textView.typeface, Typeface.NORMAL)

        if(device.name != null && device.name.isNotEmpty()) {
            holder.textView.text = device.address + "/" + device.name
        } else {
            holder.textView.text = device.address + "/" + "Unknown device"
        }
        holder.textView.setOnClickListener {
            listener.invoke(device)
        }
    }

    fun addItem(item: BluetoothDevice){
        if(!items.contains(item)) {
            items.add(item)
            notifyDataSetChanged()
        }
    }

    fun clear(){
        items.clear()
        notifyDataSetChanged()
    }


    override fun getItemCount() = items.size

    class BluetoothDeviceViewHolder(val textView: TextView) : RecyclerView.ViewHolder(textView)
}


