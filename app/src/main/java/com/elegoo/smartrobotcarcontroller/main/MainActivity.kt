package com.elegoo.smartrobotcarcontroller.main

import android.Manifest
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.PermissionChecker
import androidx.recyclerview.widget.LinearLayoutManager
import com.elegoo.smartrobotcarcontroller.bluetooth.BluetoothManager
import com.elegoo.smartrobotcarcontroller.R
import com.elegoo.smartrobotcarcontroller.common.EmptyRecyclerView
import com.elegoo.smartrobotcarcontroller.control.ControlActivity
import com.google.android.material.button.MaterialButton
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*

class MainActivity : AppCompatActivity() {

    private lateinit var bluetoothManager: BluetoothManager


    private val devicesAdapter: DevicesAdapter = DevicesAdapter { device ->
        bluetoothManager.stopScanBluetoothDevices()
        connectToDevice(device)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bluetoothManager = BluetoothManager.getInstance(this).apply {
            onScanStartedListener = { onScanStarted() }
            onScanFinishedListener = { onScanFinished() }
            onDeviceFoundListener = { device -> onDeviceFound(device) }
        }

        findViewById<EmptyRecyclerView>(R.id.rv_bluetooth_devices).apply {
            layoutManager = LinearLayoutManager(context)
            adapter = devicesAdapter
            emptyViewMessage = tv_empty_message
        }

        val btnFindDevices = findViewById<MaterialButton>(R.id.btn_find_devices)
        btnFindDevices.setOnClickListener {
            if (!packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
                Toast.makeText(this, "BLE is not supported", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (isBluetoothPermissionGranted() && isBluetoothEnabled()) {
                devicesAdapter.clear()
                bluetoothManager.startScanBluetoothDevices()
            }
        }
    }

    private fun isBluetoothPermissionGranted(): Boolean {
        return if (ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.BLUETOOTH)
                == PermissionChecker.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.BLUETOOTH_ADMIN)
                == PermissionChecker.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PermissionChecker.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.ACCESS_FINE_LOCATION)
                == PermissionChecker.PERMISSION_GRANTED) {
            true
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(
                    Manifest.permission.BLUETOOTH,
                    Manifest.permission.BLUETOOTH_ADMIN,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION), 0)
            false
        }
    }

    private fun isBluetoothEnabled(): Boolean {
        return if (bluetoothManager.isBluetoothEnabled()) {
            true
        } else {
            startActivityForResult(Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), 0)
            false
        }
    }

    private fun connectToDevice(device: BluetoothDevice) {
        val intent = Intent(this, ControlActivity::class.java)
        intent.putExtra(ControlActivity.EXTRAS_DEVICE_NAME, device.name)
        intent.putExtra(ControlActivity.EXTRAS_DEVICE_ADDRESS, device.address)
        startActivity(intent)
    }

    private fun onScanStarted() {
        btn_find_devices.text = getString(R.string.searching)
    }

    private fun onScanFinished() {
        btn_find_devices.text = getString(R.string.find_device)
    }

    private fun onDeviceFound(device: BluetoothDevice) {
        runOnUiThread {
            devicesAdapter.addItem(device)
            tv_empty_message.visibility = View.GONE
            tv_founded_devices.visibility = View.VISIBLE
        }
    }
}
