package com.elegoo.smartrobotcarcontroller.control

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.elegoo.smartrobotcarcontroller.R
import com.elegoo.smartrobotcarcontroller.bluetooth.BluetoothLeService
import com.elegoo.smartrobotcarcontroller.bluetooth.BluetoothManager
import kotlinx.android.synthetic.main.activity_control.*

class ControlActivity : AppCompatActivity() {

    companion object {
        const val TAG = "ControlActivity"
        const val EXTRAS_DEVICE_NAME = "DEVICE_NAME"
        const val EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS"

        // 1-255
        const val CAR_MAX_SPEED = 255
    }

    private lateinit var bluetoothManager: BluetoothManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_control)
        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)

        toolbar_title.text = intent.getStringExtra(EXTRAS_DEVICE_NAME)

        bluetoothManager = BluetoothManager.getInstance(this).apply {
            onGattConnectedListener = {
                runOnUiThread {
                    tv_connection_state.text = getString(R.string.connected)
                    tv_connection_state.setTextColor(ContextCompat.getColor(context, R.color.green))
                }
            }
            onGattDisconnectedListener = {
                runOnUiThread {
                    tv_connection_state.text = getString(R.string.disconnected)
                    tv_connection_state.setTextColor(ContextCompat.getColor(context, R.color.red))
                }
            }
            onBluetoothLeServiceInitFailed = {
                Toast.makeText(this@ControlActivity, getString(R.string.could_not_init_ble_service), Toast.LENGTH_LONG).show()
            }
            onCharacteristicChanged = {
                Log.d(TAG, "Input string: $it")
                // There is a possibility that we will get several pieces of data.
                // In this case get the first piece.
                val distanceToObstacle = it.split("\r")[0].toIntOrNull()
                Log.d(TAG, "Distance: $distanceToObstacle")
                onObstacleDistanceReceived(distanceToObstacle)
            }
            this.deviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS)
        }

        val gattServiceIntent = Intent(this, BluetoothLeService::class.java)
        bindService(gattServiceIntent, bluetoothManager.serviceConnection, Context.BIND_AUTO_CREATE)

        joystick.setOnMoveListener { angle, strength -> onJoystickMove(angle, strength) }
    }

    override fun onResume() {
        super.onResume()
        registerReceiver(bluetoothManager.gattUpdateReceiver, makeGattUpdateIntentFilter())
        bluetoothManager.connectLeService()
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(bluetoothManager.gattUpdateReceiver)
    }

    override fun onDestroy() {
        super.onDestroy()
        unbindService(bluetoothManager.serviceConnection)
        bluetoothManager.disconnectLeService()
    }

    private fun onObstacleDistanceReceived(distanceToObstacle: Int?) {
        if(distanceToObstacle != null) {
            when(distanceToObstacle) {
                in 0..30 -> {
                    tv_log.text = getString(R.string.obstacle_is_close)
                    tv_log.setTextColor(ContextCompat.getColor(this, R.color.red))
                }
                in 31..100 -> {
                    tv_log.text = String.format(getString(R.string.obstacle_detected), distanceToObstacle)
                    tv_log.setTextColor(ContextCompat.getColor(this, R.color.orange))
                }
                else -> {
                    tv_log.text = getString(R.string.path_clear)
                    tv_log.setTextColor(ContextCompat.getColor(this, R.color.green))
                }
            }

            val userMessage = tv_log.text.toString()
            Log.d(TAG, "Message: $userMessage")
        }
    }

    private fun makeGattUpdateIntentFilter(): IntentFilter {
        val intentFilter = IntentFilter()
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED)
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED)
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED)
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE)
        return intentFilter
    }

    private fun onJoystickMove(angle: Int, strength: Int) {
        val speed = (strength.toFloat() / 100 * CAR_MAX_SPEED).toInt()
        bluetoothManager.sendCommand(angle, speed)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}